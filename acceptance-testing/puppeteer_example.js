const puppeteer = require("puppeteer");
const testTasks = [
    "Lorem ipsum",
    "do",
    "",
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Id inventore quia maxime dolorem sint qui praesentium, pariatur sed eos repellendus similique temporibus deleniti doloremque harum! Adipisci odio incidunt esse minus!"
];
let testSuccess = true;
(async () => {
    console.info('Test start.');
    const browser = await puppeteer.launch(
        {
            "headless": false,
            "slowMo": 10
        });

    const page = await browser.newPage();

    await page.goto('http://localhost:4200/');

    for (let i = 0; i < testTasks.length; i++) {
        await page.type('[id=itemTitleBtn]', testTasks[i]);
        await page.click('#addTitleBtn');

        if (testTasks[i].length <= 3) {
            console.log("keyword must be greater than 3 characters.");
            testSuccess = false;
        }
        if (testTasks[i].length >= 100) {
            console.log("keyword must be lower than 100 characters.");
            testSuccess = false;
        }
        if (testTasks[i] == null || testTasks[i].trim() == "") {
            console.log("keyword must not be empty or null");
            testSuccess = false;
        }
    }

    if (testSuccess)
        console.log("Test successful.");
    else
        console.log("Test failed, check logs.");


    await page.screenshot({
        path: 'screenshot.png'
    });
    console.log("Test finish");
    await browser.close();
})(); { }