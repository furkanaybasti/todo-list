/*
    Copyright 2021. Furkan Aybastı All rights reserved.

    Licensed under the Apache License, Version 2.0 (the "License")
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        https://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styles: []
})
export class TodoComponent implements OnInit {
  toDoListArray: any[];
  errorMessage;
  toDoList: AngularFireList<any>;
  
  constructor(private firebasedb: AngularFireDatabase) { }

  ngOnInit() {
    this.toDoList = this.firebasedb.list('todos');
    this.toDoList.snapshotChanges()
      .subscribe(item => {
        this.toDoListArray = [];
        item.forEach(element => {
          var x = element.payload.toJSON();
          x["$key"] = element.key;
          this.toDoListArray.push(x);
        })

        this.toDoListArray.sort((a, b) => {
          return a.isChecked - b.isChecked;
        })
      });
  }

  onAdd(itemTitle) {
    if (itemTitle.value.length >= 3 && itemTitle.value.length != "") {
      this.toDoList.push({
        todo: itemTitle.value,
        isChecked: false
      });
      itemTitle.value = null;
      this.errorMessage = null;
    } else {
      this.errorMessage = "Todo sentence must be greater than 3 characters.";
    }
  }

  alterCheck($key: string, isChecked) {
    this.toDoList.update($key, { isChecked: !isChecked });
  }

  onDelete($key: string) {
    this.toDoList.remove($key);
  }
}
