/*
    Copyright 2021. Furkan Aybastı All rights reserved.

    Licensed under the Apache License, Version 2.0 (the "License")
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        https://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCv5LsJnTl1VqhM0zb8pImVPPuMhG8l4i8",
    authDomain: "todolist-72840.firebaseapp.com",
    databaseURL: "https://todolist-72840-default-rtdb.firebaseio.com",
    projectId: "todolist-72840",
    storageBucket: "todolist-72840.appspot.com",
    messagingSenderId: "655230802873",
    appId: "1:655230802873:web:49ed99ab8657b83dc97468"
  }
};
